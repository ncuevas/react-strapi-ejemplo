* Ejercicio de caracter obligatorio, para continuar con la práctica
* fecha de entrega 9/10

1) instalar strapi y añadir las entidades:
  - alumnos
  - carreras
2) agregar en el admin de strapi los tipo de usuarios:
   - admin
   - alumno
3) añadir el plugin de swagger o documentación
4) crear una app nueva de react utilizando:
   - react bootstrap
   - react router
5) La aplicación deberá permitir:
   - login con JWT y almacenar token en LocalStorage

   usuario admin:
   - abm de carreras
   - abm de alumnos
   - listado de carreras (sin paginado)
   - listado de alumnos (sin paginado)

   usuario alumno
   - ver a que carrera está asignado

6) subir la app a github o gitlab

7) deploy de aplicacion (heroku u otro servicio a definir)


https://strapi.io/documentation/3.0.0-beta.x/

instalar strapi

npm install -g yarn

1) npm install -g strapi@beta
2) strapi new server
3) cd server
4) yarn install
5) yarn develop

abrir en chrome -> http://localhost:1337/admin

- crear usuario
- loguearse
- instalar en tienda "documentacion" (si tarda y no reinicia, matar proceso y volver a ejecutar "yarn develop / npm run develop")
- crear entidades con el constructor de tipos de contenido
- modificar los roles por defecto a "admin" y "alumno", recordar asignar permisos a las entidades (pueden seleccionar todos los permisos para no tener problemas)
- agregar contenido a las entidades

- crear app de react en la misma carpeta (no adentro de server)
   comandos : 1 npx create-react-app client
              2 cd client
              3 yarn install
              4 yarn add react-router-dom
              5 yarn add react-bootstrap bootstrap
              6 yarn start


client app
- modificar index.js y agregar el import de css de bootstrap antes del "index.css"
import 'bootstrap/dist/css/bootstrap.min.css';
- modificar App.js y agregar codigo ejemplo.
- empezar a codear :=)


ADMIN STRAPI
http://localhost:1337/admin

APIS STRAPI
http://localhost:1337/entidad

SWAGGER / Documentacion API
http://localhost:1337/documentation/v1.0.0

