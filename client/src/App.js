import React, { Component } from 'react'

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

import './App.css';


class App extends Component {

 constructor(props) {
   super(props);
   this.state = {
     items: [],
     username: null,
     password: null,
   };
 }

componentDidMount () {
  const token = localStorage.getItem("token");

  if (token) {
    fetch('http://localhost:1337/careers', {
      method: 'get',
      body: null,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    })
    .then(response => response.json())
    .then(data => {
       console.log("careers", data)
    });
  }

}

logout(){
  localStorage.removeItem('token')
  localStorage.removeItem('user')
}


login (event) {


  fetch('http://localhost:1337/auth/local', {
    method: 'post',
    body: JSON.stringify(
      {
        identifier: '' || this.state.username,
        password: '' || this.state.password,
      }
    ),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  })
  .then(response => response.json())
  .then(data => {
      if (data.jwt) {
        localStorage.setItem("token", data.jwt);
        localStorage.setItem("user", JSON.stringify(data.user));
      }

  });
}


handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
}


 render() {
   return (

   <Container fluid="true">
   // aca va el header component
    <Row>
      <Col md={4}>

        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control name="username" type="email" placeholder="Username"
             onChange={this.handleInputChange.bind(this)}/>
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control name="password" type="password" placeholder="Password" onChange={this.handleInputChange.bind(this)}/>
          </Form.Group>

          <Button
            variant="primary"
            onClick={this.login.bind(this)}
          >
        loguear
        </Button>
        </Form>


        <Button
          variant="secondary"
          onClick={this.logout}
        >
        logout
        </Button>
      </Col>
    </Row>
    // aca va el footer component
  </Container>

   );
 }
}

export default App;
